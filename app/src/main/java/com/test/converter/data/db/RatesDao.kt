package com.test.converter.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single

@Dao
interface RatesDao {
    @Query("SELECT * FROM rates")
    fun getRates(): Single<List<RatesData>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRates(rates: RatesData)
}