package com.test.converter.data.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [RatesData::class],
    version = 1,
    exportSchema = false
)
abstract class RatesDatabase : RoomDatabase() {
    abstract fun ratesDao(): RatesDao
}