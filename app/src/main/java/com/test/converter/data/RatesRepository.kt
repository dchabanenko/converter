package com.test.converter.data

import com.google.gson.Gson
import com.test.converter.data.api.Rates
import com.test.converter.data.api.RatesService
import com.test.converter.data.db.RatesDao
import com.test.converter.data.db.RatesData
import io.reactivex.Observable

class RatesRepository(val ratesService: RatesService, val ratesDao: RatesDao) {
    // this is always EUR, because api is called
    // and db records are normalized by EUR
    // rebasing happens later
    private val apiBase = "EUR"

    fun getNewRates(base: String): Observable<Rates> {
        return ratesService
            .getLatest(apiBase)
            .map {
                val db = modelToDb(it)
                ratesDao.insertRates(db)

                it
            }
            .map { rebaseRates(it, base) }
    }

    fun getCachedRates(base: String): Observable<Rates> {
        return ratesDao.getRates()
            .map {
                // since only one record in db always, take first
                // potentially db can be used for rates history, etc
                val rates = dbToModel(it.first())

                rebaseRates(rates, base)
            }
            .toObservable()
    }

    private fun rebaseRates(rates: Rates, base: String): Rates {
        rates.rates.put(apiBase, 1.0)

        if (base != apiBase) {
            val ratio = rates.rates.get(base)
            if (ratio != null) {
                val rebased = mutableMapOf<String, Double>()
                rates.rates.forEach { rebased.put(it.key, it.value / ratio) }
                rates.rates = rebased
            }
        }

        return rates
    }

    private fun dbToModel(data: RatesData): Rates {
        return Gson().fromJson(data.data, Rates::class.java)
    }

    private fun modelToDb(rates: Rates): RatesData {
        return RatesData(0, Gson().toJson(rates))
    }
}