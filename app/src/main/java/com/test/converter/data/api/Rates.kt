package com.test.converter.data.api

data class Rates (
    val baseCurrency: String,
    var rates: MutableMap<String, Double>
)