package com.test.converter.data.api

import com.google.gson.GsonBuilder
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesService {
    companion object {
        private const val BASE_URL = "https://hiring.revolut.codes/api/android/"

        fun create(): RatesService {
            val interceptor = HttpLoggingInterceptor().apply {
                this.level = HttpLoggingInterceptor.Level.BODY
            }

            val client = OkHttpClient.Builder().apply {
//                if (BuildConfig.DEBUG)
//                    this.addInterceptor(interceptor)
            }.build()

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .client(client)
                .build()

            return retrofit.create(RatesService::class.java)
        }
    }

    @GET("latest")
    fun getLatest(@Query("base") base: String): Observable<Rates>
}
