package com.test.converter.data.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "rates")
data class RatesData(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    val id: Int,

    // rates of currencies in json format based on EUR
    // eg {"baseCurrency":"EUR","rates":{"AUD":1.59,"BGN":1.976,...}}
    @ColumnInfo(name = "data")
    val data: String
)