package com.test.converter.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.test.converter.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        if (savedInstanceState == null) {
            val toolbar = findViewById<Toolbar>(R.id.toolbar)
            setSupportActionBar(toolbar)

            val viewPager = findViewById<ViewPager>(R.id.pager);
            viewPager.adapter = MainAdapter(supportFragmentManager, resources)

            val tabLayout = findViewById<TabLayout>(R.id.tabs)
            tabLayout.tabGravity = TabLayout.GRAVITY_FILL;
            tabLayout.setupWithViewPager(viewPager)
        }
    }
}
