package com.test.converter.ui

import android.content.res.Resources
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.test.converter.R
import com.test.converter.ui.tabs.converter.ConverterFragment
import com.test.converter.ui.tabs.rates.RatesFragment

class MainAdapter(fm: FragmentManager, r: Resources) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private val resources = r

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> RatesFragment.newInstance()
            else -> ConverterFragment.newInstance()
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> resources.getString(R.string.rates_title)
            1 -> resources.getString(R.string.converter_title)
            else -> null
        }
    }

    override fun getCount(): Int {
        return 2
    }
}