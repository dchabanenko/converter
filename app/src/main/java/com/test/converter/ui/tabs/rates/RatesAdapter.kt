package com.test.converter.ui.tabs.rates

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.test.converter.databinding.RatesItemBinding
import io.reactivex.subjects.PublishSubject
import java.math.RoundingMode
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

class RatesAdapter(private var rates: List<Rate>) : RecyclerView.Adapter<RatesAdapter.RateHolder?>() {
    private var changed = PublishSubject.create<CharSequence>()

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): RateHolder {
        val binding = RatesItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return RateHolder(binding)
    }

    override fun onBindViewHolder(@NonNull holder: RateHolder, position: Int) {
        val rate = rates[position]

        holder.bind(rate)
    }

    override fun getItemCount(): Int {
        return rates.size
    }

    fun setRates(rates: List<Rate>, factor: Double) {
        if (this.rates.size > 1) {
            for (i in 1 until this.rates.size step 1)
                this.rates[i].value = rates[i].value * factor
            notifyItemRangeChanged(1, this.rates.size - 1)
        }
        else {
            this.rates = rates
            notifyDataSetChanged()
        }
    }

    fun getChanged(): PublishSubject<CharSequence> {
        return changed
    }

    fun updateItem(code: String, value: Double) {
        val i = rates.indexOfFirst { it.code == code }

        rates[i].value = value
        notifyItemChanged(i)
    }

    inner class RateHolder(private val binding: RatesItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(rate: Rate) {
            val format = NumberFormat.getInstance()
            format.roundingMode = RoundingMode.CEILING
            format.minimumFractionDigits = 2
            format.maximumFractionDigits = 2

            val code = rate.code
            val name = Currency.getInstance(rate.code).displayName
            val value = format.format(rate.value)

            binding.ratesItemCode.text = code
            binding.ratesItemName.text = name

            binding.ratesItemInput.setText(value)
            binding.ratesItemInput.isEnabled = rate.active
            binding.ratesItemInput.addTextChangedListener(object: TextWatcher {
                override fun afterTextChanged(s: Editable?) {}

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if (s != null && rate.active)
                        changed.onNext(s)
                }
            })

            // emitting initial value for combineLatest to initialize
            if (rates.size == 1)
                changed.onNext(rates[0].value.toString())
        }
    }
}