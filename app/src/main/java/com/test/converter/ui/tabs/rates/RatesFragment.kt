package com.test.converter.ui.tabs.rates

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.test.converter.databinding.RatesFragmentBinding
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers

class RatesFragment : Fragment() {
    private lateinit var binding: RatesFragmentBinding
    private lateinit var viewModel: RatesViewModel
    private lateinit var adapter: RatesAdapter
    private lateinit var disposable: Disposable

    private var base = "EUR"
    private var factor = 1.0

    companion object {
        fun newInstance() = RatesFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = RatesFragmentBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // initialize empty adapter with base currency only
        adapter = RatesAdapter(listOf(Rate(base, factor, true)))

        binding.ratesList.layoutManager = LinearLayoutManager(context)
        binding.ratesList.setHasFixedSize(true)
        binding.ratesList.adapter = adapter
        (binding.ratesList.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        binding.ratesList.visibility = View.INVISIBLE
        binding.ratesError.visibility = View.GONE
    }

    override fun onResume() {
        viewModel = ViewModelProvider(this).get(RatesViewModel::class.java)
        viewModel.start(base)
        disposable = Flowable.combineLatest(
            LiveDataReactiveStreams.toPublisher(viewLifecycleOwner, viewModel.rates),
            adapter.getChanged().toFlowable(BackpressureStrategy.LATEST),
            BiFunction<RateWrapper, CharSequence, RateWrapper> { rates, input ->
                factor = input.toString().toDouble()
                rates
            })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ rates ->
                rates?.data?.let { data ->
                    adapter.setRates(data, factor)
                    binding.ratesList.visibility = View.VISIBLE
                }

                if (rates?.error == null)
                    binding.ratesError.visibility = View.GONE
                else
                    binding.ratesError.visibility = View.VISIBLE
            }, {

            })
        adapter.updateItem(base, factor)

        super.onResume()
    }

    override fun onPause() {
        viewModel.clean()
        disposable.dispose()

        super.onPause()
    }
}