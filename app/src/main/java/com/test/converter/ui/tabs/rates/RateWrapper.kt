package com.test.converter.ui.tabs.rates

data class RateWrapper(
    val data: List<Rate>?,
    val error: Throwable?
)