package com.test.converter.ui.tabs.rates

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import com.test.converter.data.RatesRepository
import com.test.converter.data.api.Rates
import com.test.converter.data.api.RatesService
import com.test.converter.data.db.RatesDatabase
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class RatesViewModel(application: Application) : AndroidViewModel(application) {
    var rates : MutableLiveData<RateWrapper> = MutableLiveData()

    private var ratesRepository : RatesRepository
    private lateinit var disposable: Disposable

    init {
        val service = RatesService.create()
        val database = Room
            .databaseBuilder(application, RatesDatabase::class.java, "rates-database")
            .build()

        ratesRepository = RatesRepository(service, database.ratesDao())
    }

    fun start(base: String) {
        disposable = ratesRepository.getCachedRates(base)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result: Rates ->
                    updateRates(base, result)
                    syncRates(base)
                },
                { error: Throwable ->
                    syncRates(base)
                }
            )
    }

    private fun syncRates(base: String) {
        disposable = ratesRepository.getNewRates(base)
            .repeatWhen { completed ->
                completed.delay(1, TimeUnit.SECONDS)
            }
            .retryWhen { failed ->
                failed.flatMap { error ->
                    rates.postValue(RateWrapper(null, error))

                    Observable.timer(1, TimeUnit.SECONDS)
                }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                updateRates(base, result)
            }, { error ->
                rates.postValue(RateWrapper(null, error))
            })
    }

    private fun updateRates(base: String, result: Rates) {
        val data = result.rates.map { Rate(it.key, it.value, false) }
        val sorted = data
            .filter { it.code != base }
            .sortedWith(Comparator { r1, r2 -> r1.code.compareTo(r2.code) })
            .toMutableList()
        val first = data.firstOrNull { it.code == base }
        first?.let {
            it.active = true
            sorted.add(0, it)
        }

        rates.postValue(RateWrapper(sorted, null))
    }

    fun clean() {
        disposable.dispose()

        onCleared()
    }
}
