package com.test.converter.ui.tabs.converter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.test.converter.R

class ConverterFragment : Fragment() {
    companion object {
        fun newInstance() = ConverterFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.converter_fragment, container, false)
    }
}