package com.test.converter.ui.tabs.rates

data class Rate(
    val code: String,
    var value: Double,
    var active: Boolean
)